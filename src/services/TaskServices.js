import axios from "axios";

const updateTask = async(taskId,categoryId,token) =>{
    console.log('taskId,categoryId,token', taskId,categoryId,token)
    const response = await  axios.post('http://localhost:5000/task/v1/update',{
        taskId,
        categoryId
      },{
        headers : {'Authorization': token }
        })
    return response
}

const getAllCategory = async(token) =>{
    const response = await  axios.post('http://localhost:5000/task/v1/get-all-category',{},{
        headers : {'Authorization': token }
        })
    return response
}

const getAllTask = async(token) =>{
    const tasks = await axios.post('http://localhost:5000/task/v1/get-all',{},{
        headers : {'Authorization': token }
        })
    return tasks
}


const taskServices ={
    updateTask,
    getAllCategory,
    getAllTask
}

export default taskServices