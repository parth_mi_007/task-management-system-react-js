import axios from "axios";

const login = async ({ email, password }) => {
  const res = await axios.post("/auth/v1/login", { email, password })
  if (res.data) {
    localStorage.setItem("user", JSON.stringify(res.data.data))
  }
  return res;
}

const register = async (formData) => {
  const res = await axios.post("/auth/v1/register", formData)
  return res;
}

// Logout user
const logout = () => {
  localStorage.removeItem("user")
}

const authService = {
    register,
    logout,
    login,
}

export default authService;
