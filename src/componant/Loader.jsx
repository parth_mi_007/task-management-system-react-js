import React from 'react'
import CircleLoader from "react-spinners/CircleLoader";
export default function Loader() {
  return (
    <div
    className="flex flex-col items-center justify-center h-screen"
    data-aos="zoom-in"
  >
    <CircleLoader size={70} />
    <div className="mt-4 text-lg text-bold animate-bounce">
      Please wait for a while . . .
    </div>
  </div>
  )
}
