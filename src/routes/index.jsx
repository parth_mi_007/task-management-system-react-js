import React from 'react'
import PrivateRoutes from "./PrivateRoutes";
import PublicRoutes from "./PublicRoutes";
import DragTodo from "../views/DragTodo/DragTodo";
import Register from "../views/register/Register";
import Login from "../views/login/Login";
import { Routes, Route } from "react-router-dom";

export default function index() {
  return (
    <>
        <Routes>
        {/** Protected Routes */}
            <Route path="/" element={<PrivateRoutes/>}>
              <Route path="/" element={<DragTodo />} />
            </Route>
        {/** Public Routes */}
          <Route path="login" element={<PublicRoutes/>}>
              <Route path="/login" element={<Login/>}/>
          </Route>
          <Route path="/register" element={<Register />} />
      </Routes>
    </>
  )
}
