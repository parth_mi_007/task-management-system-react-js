import {Navigate, Outlet} from 'react-router-dom'
import { isAuth } from '../utils/Utils';

export default function PublicRoutes() {
  const auth = isAuth()
  return auth?<Navigate to="/"/>: <Outlet/>
}



