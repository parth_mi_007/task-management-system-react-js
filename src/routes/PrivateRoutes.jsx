import React from 'react'
import { isAuth } from '../utils/Utils'
import {Navigate, Outlet} from 'react-router-dom'

export default function PrivateRoutes() {
    const auth = isAuth()

  return auth ? <Outlet/> : <Navigate to="/login"/>
}
