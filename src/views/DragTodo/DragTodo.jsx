import React, { useState, useEffect } from "react";
import AOS from "aos";
import { v4 as uuidv4 } from "uuid";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import defaultImg from "../../assets/default-profile-picture.png";
//icons
import { CiDark } from "react-icons/ci";
import { AiOutlineUser } from "react-icons/ai";
import {
  IoMdAdd,
  IoIosArrowForward,
  IoIosArrowDown,
  IoIosBrowsers,
  IoMdClose,
} from "react-icons/io";
import { BsSun } from "react-icons/bs";

import "aos/dist/aos.css";
import "./drag.css";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useDispatch } from "react-redux";
import { logout } from "../../feature/ThunkFunctions/AuthThunks";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { reset } from "../../feature/Slices/taskSlice";
import {
  getCate,
  getTasks,
  updateTask,
} from "../../feature/ThunkFunctions/TaskThunks ";
import Loader from "../../componant/Loader";

export default function DragTodo() {
  const { isSuccess } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const { taskAll, category, success, isLoading, isError } = useSelector(
    (state) => state.task
  );

  const [user, setUser] = useState({});
  const [tasks, setTasks] = useState([]);
  const [taskCategory, setTaskCategory] = useState([]);
  const [profileImg, setProfileImg] = useState();

  const [columns, setColumns] = useState([]);

  // for add darkmode
  const [DarkMode, setDarkMode] = useState(false);

  //for add model state
  const [model, setModel] = useState("");

  //for task title name
  const [name, setName] = useState("");

  //for task title error
  const [nameError, setNameError] = useState();

  // collapes settings
  const [settings, setSettings] = useState([
    { id: 0, open: true },
    { id: 1, open: true },
    { id: 2, open: true },
    { id: 3, open: true },
  ]);

  //navigate
  const navigate = useNavigate();

  //update task
  const updateTaskHanler = async (taskId, categoryId, token) => {
    dispatch(updateTask({ taskId, categoryId, token }));
    getAllTask(token);
  };
  // handle dropped tasks
  const onDragEnd = (result, columns, setColumns) => {
    const { source, destination, draggableId } = result;
    if (!destination) return;
    if (source.droppableId !== destination.droppableId) {
      const taskId = draggableId;
      const categoryId = destination.droppableId;
      updateTaskHanler(taskId, categoryId, user.token);
    }
  };

  // handle add modal
  const handleModel = (index) => {
    setModel(index);
    if (index === model) {
      setModel("");
    }
  };

  // handle dark mode
  const onDarkMode = () => setDarkMode(!DarkMode);

  useEffect(() => {
    if (isSuccess) {
      toast.success("logged out successfully");
      navigate("/login");
    }
    dispatch(reset());
  }, [isSuccess]);

  //logout
  const onLogout = () => {
    dispatch(logout());
    dispatch(reset());
  };
  const getAllTask = (token) => {
    dispatch(getCate(token));
    dispatch(getTasks(token));
  };

  // handle new tasks and add new task
  const handleSaveTask = (id) => {
    if (name === "") {
      return setNameError("Title is required");
    }
    setNameError();
    const newArray = {
      ...columns,
      [id]: {
        ...columns[id],
        items: [...columns[id].items, { id: uuidv4(), content: name }],
      },
    };
    setName("");
    setColumns(newArray);
    setModel("");
  };

  //useEffect for profile
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"));
    setUser(user);
    if (user?.profile?.includes("googleuser")) {
      setProfileImg(user.profile);
    } else {
      const profile = `http://localhost:5000/${user.profile}`;
      setProfileImg(profile);
    }
    getAllTask(user.token);
  }, []);

  // useEffect for handling dark mode
  useEffect(() => {
    if (DarkMode) localStorage.setItem("theme", "dark");
    else localStorage.setItem("theme", "light");

    if (
      localStorage.theme === "dark" ||
      (!("theme" in localStorage) &&
        window.matchMedia("(prefers-color-scheme: dark)").matches)
    ) {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  }, [DarkMode]);

  useEffect(() => {
    if (success) {
      setTasks(taskAll);
      dispatch(reset());
      setTaskCategory(category);
      dispatch(reset());
    }
  }, [success, category]);

  // get task title
  const handleChange = (event) => setName(event.target.value);

  // handle  collapse settings
  const toggle = (index) => {
    setSettings((previousState) =>
      previousState.map((item) => {
        if (item.id === index) {
          return { ...item, open: !item.open };
        } else {
          return { ...item };
        }
      })
    );
  };

  //useEffect for AOS (animate on scroll)
  useEffect(() => {
    AOS.init({ duration: 1500 });
  }, []);

  // if (isLoading) {
  //   return (
  //     <Loader/>
  //   )
  // }

  return (
    <>
      {/* main content */}
      <div
        className={`h-screen bg-[url('assets/back.jpg')] bg-center overflow-x-hidden`}
      >
        <div className="h-screen bg-slate-600/50 dark:bg-slate-800 p-6">
          <div
            className="text-right pb-3"
            data-aos="fade-left"
            data-aos-duration="2000"
          >
            <div>
              <button
                className={`px-4 py-2 rounded-full text-black bg-gray-200 font-semibold text-sm sm:text-base shadow dark:bg-slate-100 dark:text-black`}
                onClick={(e) => {
                  onDarkMode();
                }}
              >
                <div className="flex items-center font-medium sm:font-semibold">
                  <div> {DarkMode ? "Light Mode" : "Dark Mode"} </div>
                  <div className="ml-2">
                    {DarkMode ? <BsSun /> : <CiDark />}
                  </div>
                </div>
              </button>
            </div>
          </div>
          <div
            className="container container-height mx-auto bg-gray-200 dark:bg-slate-400 rounded-xl shadow border p-8"
            data-aos="fade-down"
          >
            <div className="flex justify-between">
              <p className="text-base text-gray-700 font-bold mb-3 dark:text-gray-800 sm:text-3xl">
                Welcome, {user?.firstName}! 🚀
              </p>
              <div
                className="w-[50px] h-[50px]"
                type="button"
                data-bs-toggle="offcanvas"
                data-bs-target="#staticBackdrop"
                aria-controls="staticBackdrop"
              >
                <img
                  src={profileImg ? profileImg : defaultImg}
                  className="object-cover rounded-[50%] w-full h-full border-gray-400 border-[1px]"
                  referrerPolicy="no-referrer"
                />
              </div>
            </div>
            <p className="mb-3 text-gray-500 text-sm sm:text-lg  sm:mb-6 dark:text-white">
              Here is the Task List
            </p>
            <div
              class="offcanvas offcanvas-start bg-slate-300"
              data-bs-backdrop="static"
              tabIndex="-1"
              id="staticBackdrop"
              aria-labelledby="staticBackdropLabel"
            >
              <div class="offcanvas-header">
                <div>
                  <p
                    class="offcanvas-title uppercase text-[25px] p-0 m-0 flex items-center"
                    id="staticBackdropLabel"
                  >
                    Profile <AiOutlineUser className="text-[24px] ml-3" />
                  </p>
                </div>
                <div>
                  <button
                    type="button"
                    data-bs-dismiss="offcanvas"
                    aria-label="Close"
                  >
                    <IoMdClose className="text-[25px]" />
                  </button>
                </div>
              </div>
              <div class="offcanvas-body">
                <div>
                  <div className="flex items-start justify-center mb-5">
                    <div className="w-[130px] h-[130px]">
                      <img
                        src={profileImg ? profileImg : defaultImg}
                        className="w-auto object-cover h-full rounded-[50%]"
                        alt="profile-pic"
                        referrerPolicy="no-referrer"
                      />
                    </div>
                  </div>
                  <div className="">
                    <table className="table">
                      <tr>
                        <td className="px-1 capitalize whitespace-nowrap">
                          Full Name :
                        </td>
                        <td className="p-2 capitalize whitespace-nowrap">
                          {user.firstName} {user.lastName}
                        </td>
                      </tr>
                      <tr>
                        <td className="px-3 capitalize whitespace-nowrap">
                          Email :
                        </td>
                        <td className="p-1 whitespace-nowrap">{user.email}</td>
                      </tr>
                    </table>
                  </div>
                  <div className="text-center mt-5">
                    <button
                      className="px-4 py-2 bg-orange-400 rounded-sm uppercase"
                      onClick={() => onLogout()}
                    >
                      logout
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex overflow-x-auto pb-4 mt-5 min-h-[280px]">
              <DragDropContext
                onDragEnd={(result) => onDragEnd(result, columns, setColumns)}
              >
                {taskCategory?.map((item, index) => {
                  return (
                    <>
                      <Droppable droppableId={item._id} key={item._id}>
                        {(provided, snapshot) => {
                          return (
                            <>
                              <div
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                className={`  ${
                                  settings.find((item) => item.id === index)
                                    .open
                                    ? " bg-blue-200 overflow-y-auto  dark:bg-slate-700 h-[250px] sm:h-[375px] sm:w-[200px] mx-3 pt-5 min-w-[260px] "
                                    : "rotate-90 bg-blue-200  dark:bg-slate-700 h-[0px] sm:h-[0px] sm:w-[250px] mt-[125px] min-w-[240px] ml-[-130px] mr-[-50px]"
                                }
                                    `}
                                key={item._id}
                              >
                                <div className="p-2 bg-slate-700 text-blue-50 text-center  uppercase mt-[-48px] sm:w-[100%] sm:text-sm md:text-lg text-sm flex items-center justify-between sticky top-[-49px] z-50">
                                  <div
                                    className="bg-slate-100 text-dark rounded-sm p-1"
                                    onClick={() => toggle(index)}
                                  >
                                    {settings.find((item) => item.id === index)
                                      .open ? (
                                      <IoIosArrowDown />
                                    ) : (
                                      <IoIosArrowForward />
                                    )}
                                  </div>
                                  <div className="text-base">{item.title}</div>
                                  <div className="flex items-center ">
                                    <div className=" flex items-center mx-2">
                                      <IoIosBrowsers />
                                      <p className="mx-1">
                                        {
                                          tasks?.filter(
                                            (task) => task.category === item._id
                                          ).length
                                        }
                                      </p>
                                    </div>
                                    <div
                                      className={`${
                                        settings.find(
                                          (item) => item.id === index
                                        ).open
                                          ? ""
                                          : "hidden"
                                      } text-blue-50 border p-1`}
                                      key={item._id}
                                      onClick={(e) => handleModel(index)}
                                    >
                                      <IoMdAdd className="" />
                                    </div>
                                  </div>
                                </div>
                                {settings.find((item) => item.id === index)
                                  .open && (
                                  <div
                                    data-aos="fade-down"
                                    data-aos-duration="500"
                                  >
                                    {model === index && (
                                      <div
                                        data-aos="zoom-in-down"
                                        data-aos-duration="500"
                                      >
                                        <div class="form-group m-3 p-3 dark:border-white  border-slate-600 border-2 rounded-md">
                                          <p className="dark:text-blue-50 text-sm mb-[5px]">
                                            Title
                                          </p>
                                          <input
                                            type="text"
                                            className="form-control border-slate-600 p-[1px] focus:ring-0 focus:border-black"
                                            value={name}
                                            onChange={handleChange}
                                            id="exampleInputPassword1"
                                            placeholder=""
                                          />
                                          {nameError ? (
                                            <div className="text-red-600 text-sm">
                                              {" "}
                                              {nameError}{" "}
                                            </div>
                                          ) : null}
                                          <div className="flex justify-around mt-3">
                                            <div>
                                              {" "}
                                              <button
                                                className="px-2 mx-1 py-1 bg-blue-400 rounded text-sm text-slate-50"
                                                onClick={() =>
                                                  handleSaveTask(item._id)
                                                }
                                              >
                                                {" "}
                                                Save{" "}
                                              </button>
                                            </div>
                                            <div>
                                              {" "}
                                              <button
                                                className="px-2 py-1 bg-red-600 rounded text-white text-sm"
                                                onClick={() => setModel("")}
                                              >
                                                {" "}
                                                Cancel{" "}
                                              </button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    )}
                                    {tasks.map((task, index) => {
                                      if (task.category === item._id) {
                                        return (
                                          <>
                                            <Draggable
                                              key={task._id}
                                              draggableId={task._id}
                                              index={index}
                                            >
                                              {(provided, snapshot) => {
                                                return (
                                                  <>
                                                    <div
                                                      ref={provided.innerRef}
                                                      {...provided.draggableProps}
                                                      {...provided.dragHandleProps}
                                                      style={{
                                                        userSelect: "none",
                                                        ...provided
                                                          .draggableProps.style,
                                                      }}
                                                      className="m-3 text-stone-50 p-2 dark:bg-slate-300 dark:text-slate-900 bg-slate-600  sm:p-3 sm:text-base text-sm border-6 border-red-700 border-double"
                                                    >
                                                      {task.title}
                                                    </div>
                                                  </>
                                                );
                                              }}
                                            </Draggable>
                                          </>
                                        );
                                      }
                                    })}
                                  </div>
                                )}
                                {provided.placeholder}
                              </div>
                            </>
                          );
                        }}
                      </Droppable>
                    </>
                  );
                })}
              </DragDropContext>
            </div>
          </div>
        </div>
      </div>     
    </>
  );
}
