import React, { useEffect, useState } from "react";
import {FaUserAlt,FaLock,FaKey,FaAt} from 'react-icons/fa'
import {IoMdMail} from 'react-icons/io'
import {BsFillImageFill} from 'react-icons/bs'
import sideImg from "../../assets/undraw_welcome_cats_thqn.svg";
import axios from 'axios'
import { useGoogleLogin } from '@react-oauth/google'
import {FcGoogle} from 'react-icons/fc'
import { Link,useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form"
import { useDispatch, useSelector } from "react-redux";
import { register as registerMain } from "../../feature/ThunkFunctions/AuthThunks";
import { reset } from "../../feature/Slices/userSlice";

export default function Register() {
  const navigate =useNavigate()
  const dispatch =useDispatch()
  const {isSuccess} = useSelector((state) => state.user)

  useEffect(()=>{
    if(localStorage.getItem('user')){
      navigate('/')
    }
  },[])

  const googleRegister = useGoogleLogin({
    onSuccess: async tokenResponse => {
      // fetching userinfo can be done on the client or the server
      const userInfo = await axios
        .post('http://localhost:5000/auth/v1/google-register',{
          code : tokenResponse.code
        })
        .then(res => {
          if(res.data){
            navigate('/login')
          }
        })
    },
    flow: 'auth-code',
  })

const onSubmit = async(data) =>{
  const formData = new FormData()
  formData.append('firstName', data.firstName)
  formData.append('lastName', data.lastName)
  formData.append('email', data.email)
  formData.append('password', data.password)
  formData.append('profile', data.profile[0])
  
  dispatch(registerMain(formData))
  //     navigate('/login')
}
useEffect(()=>{
  if(isSuccess){
    navigate('/login')
  }
  dispatch(reset())
},[isSuccess])

const { register,handleSubmit,formState: { errors },watch } = useForm()
return (
  <>
      <section class="h-screen">
        <div className="h-screen flex justify-center items-center">
          <div className="shadow-lg p-3 ml-[30px]">
          <form  onSubmit={handleSubmit(onSubmit)}>
            <div className="text-3xl text-center mb-5 text-[#6c63ff]">WELCOME  |  SIGN UP</div>
           <div className="row">
            <div className="col-md-6">
            <div className="flex items-center mt-3">
              <label className="mr-3 text-[23px]"><FaUserAlt/></label>
              <div className="w-full">
                <input
                  class="shadow w-full appearance-none py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-[1px] border-gray-300"
                  type="text"
                  {...register('firstName',{required:"First name is required."})}
                  placeholder="Your First Name"
                />
              {errors.firstName ? <div className="text-red-700"> {errors.firstName.message}</div> : null}
              </div>
            </div>
            <div className="flex items-center mt-3">
              <label className="mr-3 text-[23px]"><FaAt/></label>
              <div className="w-full">
              <input
                class="shadow w-full appearance-none py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-[1px] border-gray-300"
                id="username"
                type="text"
                {...register('lastName',{required:"Last name is required."})}
                placeholder="Your Last Name"
              />
              {errors.lastName ? <div className="text-red-700"> {errors.lastName.message}</div> : null}

              </div>
            </div>
            <div className="flex items-center mt-3">
              <label className="mr-3 text-[23px]"><IoMdMail/></label>
              <div className="w-full">
                <input
                  class="shadow w-full appearance-none py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-[1px] border-gray-300"
                  id="username"
                  type="text"
                  {...register('email',{required: 'Email is required.', pattern:{value:/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, message:'enter valid email'}})}
                  placeholder="Your Email"
                />
              {errors.email ? <div className="text-red-700"> {errors.email.message}</div> : null}

              </div>
            </div>

           
            </div>
            <div className="col-md-6">
            <div className="flex items-center mt-3">
              <label className="mr-3 text-[23px]"><FaLock/></label>
              <div className="w-full">
              <input
                class="shadow w-full appearance-none py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-[1px] border-gray-300"
                id="username"
                type="password"
                placeholder="Your Password"
                {...register('password',{required:"Password is required."})}
              />
              {errors.password ? <div className="text-red-700"> {errors.password.message}</div> : null}
              
              </div>
             
            </div>
            <div className="flex items-center mt-3">
              <label className="mr-3 text-[23px]"><FaKey/></label>
              <div className="w-full">
                <input
                  class="shadow w-full appearance-none py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-[1px] border-gray-300"
                  id="username"
                  type="password"
                  placeholder="Your Confirm Password"
                  {...register('cPassword',{required:"Confirm password is required."})}
                />
                {errors.cPassword ? <div className="text-red-700"> {errors.cPassword.message}</div> : null}
              </div>
            </div>
            <div className="flex items-center mt-3">
              <label className="mr-3 text-[23px]"><BsFillImageFill/></label>
              <input
                class="shadow w-full appearance-none py-2 px-3 focus:outline-none focus:shadow-outline border-[1px] border-gray-300 hidden"
                id="img"
                type="file"
                accept="image/*"
                {...register('profile',{required:'profile is required.'})}
              />
              <div className="w-full">
              <label htmlFor='img' className="text-gray-400 w-full border-[1px] border-gray-300 py-2 px-3 cursor-pointer">
                {!watch('profile') || watch('profile').length == 0 ? 'Your profile' : <div className="text-blue-700">
                {watch('profile')[0].name +"  "+ 'uploaded'}
                </div> }
              </label>
              {errors.profile ? <div className="text-red-700"> {errors.profile.message}</div> : null}
              </div>
             
            </div>
            </div>

           </div>
            
             {/* login with google */}
             <div>
                <div className="text-center mt-5">
                    <div className="text-center relative">
                        <div className="">
                            <span className="text-gray-400 cursor-pointer bg-white px-3 text-sm">Or Signup Using</span>
                        </div>
                        <hr className="text-black w-full absolute top-[12px] z-[-1]" />
                    </div>
                    <div className="text-center mt-3 mb-3">
                    <button onClick={() => googleRegister()}>
                        <FcGoogle className="text-[30px]"/>
                    </button>
                    </div>
                </div>
            </div>
            
            {/* login button */}
            <div className="">
                <button
                  class=" w-100 shadow bg-[#6c63ff] focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
                  type="submit"
                >
                  SIGN UP
                </button>
            </div>

            <div>
                <div className="flex mt-4 justify-center">
                    <p className="mr-2 capitalize">Already registered?</p>
                    <Link to='/login' className="text-blue-700 hover:text-blue-900 cursor-pointer uppercase ">SIGNIN</Link>
                </div>
            </div>
          
          </form>
          </div>
        </div>
      </section>
    </>
  );
}
