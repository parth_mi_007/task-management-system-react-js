import React, { useEffect } from "react";
import { useGoogleLogin } from '@react-oauth/google'
import {FcGoogle} from 'react-icons/fc'
import sideImg from "../../assets/undraw_secure_login_pdn4.svg";
import axios from 'axios'
import { Link, useNavigate } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import { useForm } from "react-hook-form"
import { useDispatch,useSelector } from "react-redux";
import { loginUser } from "../../feature/ThunkFunctions/AuthThunks";
import { reset } from "../../feature/Slices/userSlice";

export default function Login() {

  const navigate = useNavigate()
  const dispatch = useDispatch()
  const {userInfo, isLoading, isError, isSuccess, message} = useSelector((state) => state.user)
  const { register, handleSubmit, formState:{errors}} = useForm()
//google login handler
const googleLogin = useGoogleLogin({
    onSuccess: async tokenResponse => {
      // fetching userinfo can be done on the client or the server
      const userInfo = await axios
        .post('http://localhost:5000/auth/v1/google-login',{
          code : tokenResponse.code
        })
        .then(({data}) => {
          const user = data.data
          localStorage.setItem('user',JSON.stringify(user))
          toast.success("Logged in successfully !", { theme:'dark'})
          navigate('/')
        })
    },
    flow: 'auth-code',
  })
//login handler
  const onSubmitForm = async(data) =>{
    dispatch(loginUser(data))
}

  useEffect(() => {
    if (isError) {
      toast.error(message)
    }
    if (isSuccess && userInfo) {
      toast.success(message)
      navigate('/')
    }
    dispatch(reset())
  }, [userInfo, isError, isSuccess, message, navigate, dispatch])

if (isLoading) {
    return <div>loading.....</div>
}

  return (
    <>

      <section class="h-screen">
        <div className="h-screen flex justify-center items-center p-[70px]">
          <div className="mr-[50px] w-[50%]">
            <img src={sideImg} />
          </div>
          <div className="shadow-lg p-[50px] w-[50%] ml-[30px]">
            <form onSubmit={handleSubmit(onSubmitForm)}>
            <div className="text-3xl mb-5 text-[#6c63ff]">Login</div>
            <div>
              <label className="mb-2">Email</label>
              <input
                class="shadow appearance-none border  w-full py-2 px-3 text-gray-700 leading-tight focus:outline-[#6c63ff] focus:shadow-outline"
                id="username"
                type="text"
                placeholder="Your Email"
                {...register('email',{required: 'Email is required.', pattern:{value:/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, message:'Enter valid email'}})}
              />
              {errors.email ? <div className="text-red-700"> {errors.email.message}</div> : null}
            </div>
            <div className="mt-4">
              <label className="mb-2">Password</label>
              <input
                class="shadow appearance-none border w-full py-2 px-3 text-gray-700 leading-tight focus:outline-[#6c63ff] focus:shadow-outline"
                id="username"
                type="Password"
                placeholder="Your Password"
                {...register('password',{required:'Password is required.'})}
              />
              {errors.password ? <div className="text-red-700"> {errors.password.message}</div> : null}

            </div>
            <div class="flex items-center">
              <input
                id="bordered-checkbox-1"
                type="checkbox"
                value=""
                name="bordered-checkbox"
                class="w-5 h-4 text-blue-600 bg-gray-100 rounded border-gray-300  dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:bg-gray-700 dark:border-gray-600"
                // {...register('remember')}
              />
              <label
                htmlFor="bordered-checkbox-1"
                class="py-4 ml-2 text-sm text-base capitalize text-gray-900 dark:text-gray-300"

              >
                remember me
              </label>
            </div>
            {/* login button */}
            <div className="">
              <div className="mt-2">
                <button
                  class=" w-100 shadow bg-[#6c63ff] focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
                  type="submit"
                >
                  Log In
                </button>
       
              </div>
            </div>
            </form>

            {/* login with google */}
            <div>
                <div className="text-center mt-4">
                    <div className="text-center relative">
                        <div className="">
                            <span className="text-gray-400 cursor-pointer bg-white px-3 text-sm">Or Signin Using</span>
                        </div>
                        <hr className="text-black w-full absolute top-[12px] z-[-1]" />
                    </div>
                    <div className="text-center mt-3">
                    <button onClick={() => googleLogin()}>
                        <FcGoogle className="text-[30px]"/>
                    </button>
                    </div>
                </div>
            </div>

            {/* register link */}
            <div>
                <div className="flex mt-4 justify-center">
                    <p className="mr-2 capitalize">Don't have account?</p>
                    <Link to='/register' className="text-blue-700 hover:text-blue-900 cursor-pointer uppercase ">Signup</Link>
                </div>
            </div>
          </div>
        </div>
      </section>

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark"
      />
    </>
  );
}
