import { createAsyncThunk } from "@reduxjs/toolkit";
import taskService from "../../services/TaskServices"

export const updateTask = createAsyncThunk(
  "task/update",
  async (data, { rejectWithValue }) => {
    try {
      const { taskId,categoryId,token } = data
      console.log('res', taskId, categoryId, token)
      const res = await taskService.updateTask(taskId,categoryId,token)
      return res.data.data
    } catch (error) {
      console.log('in api',error)
      return rejectWithValue(error.message)
    }
  }
)

export const getCate = createAsyncThunk(
  "get/category",
  async (token, { rejectWithValue }) => {
    try {
      const res = await taskService.getAllCategory(token)
      return res.data.data
    } catch (error) {
      return rejectWithValue(error.message)
    }
  }
)

export const getTasks = createAsyncThunk(
  "get/tasks",
  async (token, { rejectWithValue }) => {
    try {
      const res = await taskService.getAllTask(token)
      return res.data.data
    } catch (error) {
      return rejectWithValue(error.message)
    }
  }
)


