import { createAsyncThunk } from "@reduxjs/toolkit";
import authService from "../../services/AuthServices"

export const loginUser = createAsyncThunk(
  "auth/login",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      const res = await authService.login({email, password})
      return res.data.data
    } catch (error) {
      return rejectWithValue(error.message)
    }
  }
)

export const register = createAsyncThunk(
  "auth/register",
  async (formData, { rejectWithValue }) => {
    try {
      const res = await authService.register(formData)
      return res.data.data
    } catch (error) {
      return rejectWithValue(error.message)
    }
  }
)


export const logout = createAsyncThunk('auth/logout', async () => {
  authService.logout()
})