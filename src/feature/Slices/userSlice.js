import { createSlice } from "@reduxjs/toolkit"
import {loginUser,logout, register} from "../ThunkFunctions/AuthThunks"

// Get user from localStorage
const user = JSON.parse(localStorage.getItem('user'))

const initialState = {
    userInfo: user ? user : null,
    isSuccess: false,
    isError: false,
    isLoading: false,
    message: ''
}

const taskSlice = createSlice({
    name: 'Tasks',
    initialState,
    reducers: {
       reset:(state)=> {
        state.isError = false
        state.isSuccess = false
        state.isLoading=false
        state.message = ''
       }
    },
    extraReducers:{
        [loginUser.pending](state){
            state.isLoading = true;
        },
        [loginUser.fulfilled](state, action) {
            state.isLoading = false;
            state.userInfo = action.payload;
            state.isSuccess = true;
            state.message = "Logged in successfully"
        },
        [loginUser.rejected](state, action) {
            state.isError = true;
            state.message = action.payload;
        },
        [logout.fulfilled](state, action) {
            state.userInfo = null;
            state.isSuccess = true;
            state.message = "Logged out successfully"
        },
        [register.pending](state){
            state.isLoading = true;
        },
        [register.fulfilled](state, action) {
            state.isLoading = false;
            state.isSuccess = true;
            state.message = "register successfully"
        },
        [register.rejected](state, action) {
            state.isError = true;
            state.message = action.payload;
        },
    }
})
export const {reset} = taskSlice.actions
export default taskSlice.reducer