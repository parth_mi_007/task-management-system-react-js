import { createSlice } from "@reduxjs/toolkit"
import { updateTask, getTasks, getCate} from '../ThunkFunctions/TaskThunks '
const initialState = {
    taskAll :[],
    category:[],
    success: false,
    isError: false,
    isLoading: false
}

const authSlice = createSlice({
    name: 'Auth',
    initialState,
    reducers: {
       reset:(state)=> {
        state.isError = false
        state.success = false
        state.isLoading=false
        state.message = ''
       }
    },
    extraReducers:{
        [getCate.pending](state){
            state.isLoading = true;
        },
        [getCate.fulfilled](state, action) {
            state.isLoading = false;
            state.category = action.payload;
            state.success = true;
        },
        [getCate.rejected](state, action) {
            state.isError = true;
            state.message = action.payload;
        },
        [getTasks.pending](state){
            state.isLoading = true;
        },
        [getTasks.fulfilled](state, action) {
            state.isLoading = false;
            state.taskAll = action.payload;
            state.success = true;
        },
        [getTasks.rejected](state, action) {
            state.isError = true;
            state.message = action.payload;
        },
        [updateTask.pending](state){
            state.isLoading = true;
        },
        [updateTask.fulfilled](state, action) {
            state.isLoading = false;
            state.isSuccess = true;
        },
        [updateTask.rejected](state, action) {
            state.isError = true;
            state.message = action.payload;
        },
    }
})
export const {reset} = authSlice.actions
export default authSlice.reducer