import { configureStore } from '@reduxjs/toolkit'
import UserReducer from  '../feature/Slices/userSlice'
import taskReducer from '../feature/Slices/taskSlice'

const store = configureStore({
    reducer : {
        user : UserReducer,
        task: taskReducer
    }
})

export default store